package com.sepoe.fraction;

import org.junit.Test;

import static com.sepoe.fraction.Fraction.fraction;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Class to test class Fraction.
 *
 * @author lumiha
 * @since 04/05/15.
 *
 */
public class FractionTest {


    @Test
    public void createFraction_fromSimpleString_returnsFractionObject() {
        assertThat(fraction("2"),     is(fraction(2)) );
        assertThat(fraction("3"),     is(fraction(3)) );
        assertThat(fraction("1/2"),   is(fraction(1,2)) );
        assertThat(fraction("-1/2"),  is(fraction(-1,2)) );
        assertThat(fraction("1/-2"),  is(fraction(-1,2)) );
        assertThat(fraction("-1/-2"), is(fraction(1,2)) );
    }

    @Test
    public void createFraction_viaInvalidString_throwsException() {
        try {
            fraction("x");
            fail("x should not be accepted as input fraction");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), containsString("x"));
        }

        try {
            fraction("");
            fail("empty string should not be accepted as input fraction");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), containsString("empty"));
        }

        try {
            fraction(null);
            fail("null should not be accepted as input fraction");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), containsString("null"));
        }
    }

    @Test
    public void printingFraction_withInteger_showsInteger() {
        assertThat(fraction(6).toString(), is("6"));
        assertThat(fraction(42).toString(), is("42"));
    }

    @Test
    public void printingFraction_with1div2_printsFractionNotation() {
        assertThat(fraction(1, 2).toString(), is("1/2"));
    }

    @Test
    public void print_improperFraction_asIs() {
        assertThat(fraction(7,5).toString(), is("7/5"));
    }

    @Test
    public void adding_twoInteger_resolvesToInteger() {
        assertThat(fraction(4).add(fraction(9)), is(fraction(13)));
    }

    @Test
    public void adding_twoFractions_resultInFraction() {
        assertThat(fraction(7,3).add(fraction(4,5)), is(fraction(47,15)) );
    }

    @Test
    public void express_fractionWithNumerator0_asFraction_0() {
        assertThat(fraction(0), is(fraction(0, 1221342)));
    }

    @Test
    public void expressFractions_givenNotInLowestTerms_inLowestTerms() {
        assertThat(fraction(4, 6), is(fraction(2,3)));
        assertThat(fraction(3, 9), is(fraction(1,3)));
    }

    @Test
    public void multiplying_twoFractions_resultsInCorrectFraction() {
        assertThat(fraction(1,3).multiplyBy(fraction(1, 2)), is(fraction(1,6)));
        assertThat(fraction(2,5).multiplyBy(fraction(3, 2)), is(fraction(3,5)));
    }

    @Test
    public void subtracting_twoFractions_resultInFraction() {
        assertThat(fraction(1,2).subtract(fraction(1, 3)), is(fraction(1,6)));
        assertThat(fraction(1,3).subtract(fraction(1, 3)), is(fraction(0)));
        assertThat(fraction(1,3).subtract(fraction(1, 2)), is(fraction(-1,6)));
    }

    @Test
    public void flip_fraction_resultsInReciprocal() {
        assertThat(fraction(2,3).flip(), is(fraction(3,2)));
        assertThat(fraction(1,3).flip(), is(fraction(3)));
    }

    @Test
    public void dividing_twoFractions_resultsInFraction() {
        assertThat(fraction(1,2).divideBy(fraction(1, 3)), is(fraction(3,2)));
    }


    @Test
    public void creatingFraction_withSignumOnValues_resultsInFractionWithSignumOnNumerator() {
        assertThat(fraction(1,-2), is(fraction(-1,2)));
        assertThat(fraction(-1,2), is(fraction(-1,2)));
        assertThat(fraction(-1,-2), is(fraction(1,2)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFraction_with_0_Denominator_throwsException() {
        fraction(1,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void flip_for_0_throwsException() {
        fraction(0,1).flip();
    }

    @Test(expected = IllegalArgumentException.class)
    public void division_by_0_throwsException() {
        fraction(1,1).divideBy(fraction(0, 1));
    }

    @Test
    public void equals_forTwoEqualFractions_returnTrue() {
        assertThat(fraction(1,1).equals(fraction(1, 1)), is(true));
    }

    @Test
    public void equals_forNumberAndEqualFraction_returnTrue() {
        assertThat(fraction(3).equals(fraction(3, 1)), is(true));
    }

    @Test
    public void equals_forTwoDifferentFractions_returnFalse() {
        assertThat(fraction(1,2).equals(fraction(1, 1)), is(false));
        assertThat(fraction(2,2).equals(fraction(1, 2)), is(false));
    }

    @SuppressWarnings("ObjectEqualsNull")
    @Test
    public void equals_forComparingWithNull_returnsFalse() {
        assertThat(fraction(2).equals(null), is(false));
    }

    /**
     * This test would be nice to test by property based testing. (e.g. ScalaCheck)
     * */
    @Test
    public void hashCode_forTwoEqualFractions_areEqual() {
        assertThat(fraction(1,2).hashCode(), is(fraction(1,2).hashCode()));
        assertThat(fraction(5, 2).hashCode(), is(fraction(5, 2).hashCode()));
    }

}
