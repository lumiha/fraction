package com.sepoe.fraction;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.valueOf;

/**
 * Class representing fractions.
 * <p/>
 * Requested by the TDD course from J.B. Rainsberger http://online-training.jbrains.ca/courses/wbitdd-01/
 *
 * @author L.Hankewitz
 * @since 04.05.2015
 */
public class Fraction {
    private static final String FRACTION_PATTERN = "(-?\\d+)|(-?\\d+)/(-?\\d+)";
    private int numerator;
    private int denominator;

    /**
     * Create new fraction with numerator number and denominator 1.
     *
     * @param number as the new numerator of the created fraction
     */
    public static Fraction fraction(final int number) {
        return new Fraction(number);
    }

    /**
     * Create new fraction with numerator and denominator.
     *
     * @param numerator   of the created fraction
     * @param denominator of the created fraction
     */
    public static Fraction fraction(final int numerator, final int denominator) {
        return new Fraction(numerator, denominator);
    }

    /**
     * Create fraction for integer.
     *
     * @param number representing the numerator of the resulting fraction with denominator 1.
     */
    private Fraction(final int number) {
        this(number, 1);
    }


    /**
     * Create fraction from a given string in the format [num]|[num]/[num].
     *
     * @param input a string representation of a fraction in the defined format
     * @throws java.lang.IllegalArgumentException when the input is not a valid fraction
     */
    public static Fraction fraction(final String input) {
        if (input == null) throwException(null);

        final Matcher fractionMatcher = getFractionMatcher(input);

        if (!fractionMatcher.matches()) throwException(input);

        return extractFraction(fractionMatcher);
    }

    private static Matcher getFractionMatcher(final String fractionAsString) {
        final Pattern fractionStringPattern = Pattern.compile(FRACTION_PATTERN);
        return fractionStringPattern.matcher(fractionAsString);
    }

    private static Fraction extractFraction(final Matcher fractionMatcher) {
        final Fraction result;
        final String numeratorOfGroup1 = fractionMatcher.group(1);
        if (numeratorOfGroup1 != null) {
            result = fraction(valueOf(numeratorOfGroup1));
        } else {
            final String numeratorOfGroup2 = fractionMatcher.group(2);
            final String denominatorOfGroup2 = fractionMatcher.group(3);
            result = fraction(valueOf(numeratorOfGroup2), valueOf(denominatorOfGroup2));
        }
        return result;
    }

    private static void throwException(final String input) {
        final String inputAsString;
        if (input == null) {
            inputAsString = "null";
        } else if (input.isEmpty()) {
            inputAsString = "empty string";
        } else {
            inputAsString = input;
        }

        throw new IllegalArgumentException(inputAsString + " is not a valid fraction in the format <num>|<num>/<num>");
    }

    /**
     * Create fraction.
     *
     * @param numerator   of the fraction
     * @param denominator of the fraction
     * @throws IllegalArgumentException when denominator is 0
     */
    private Fraction(final int numerator, final int denominator) {
        if (denominator == 0) throw new IllegalArgumentException("Division by 0");

        final int signumOfDenominator = (denominator < 0) ? -1 : 1;
        this.numerator = numerator * signumOfDenominator;
        this.denominator = Math.abs(denominator);

        transformToLowestTerms();
    }

    /**
     * Get the result of the adding the passed fraction to this.
     *
     * @param fraction to add
     * @return result of the addition
     */
    public Fraction add(final Fraction fraction) {
        return new Fraction((this.numerator * fraction.denominator) + (this.denominator * fraction.numerator)
                , this.denominator * fraction.denominator);
    }

    /**
     * Get the multiplication of the called fraction and the given fraction.
     *
     * @param fraction to multiply this with.
     * @return result of the multiplication
     */
    public Fraction multiplyBy(final Fraction fraction) {
        return new Fraction(this.numerator * fraction.numerator, this.denominator * fraction.denominator);
    }

    /**
     * Get the subtraction of the called fraction and the given fraction.
     *
     * @param fraction to subtract from this.
     * @return result of subtraction
     */
    public Fraction subtract(final Fraction fraction) {
        return new Fraction((this.numerator * fraction.denominator) - (fraction.numerator * this.denominator)
                , this.denominator * fraction.denominator);
    }

    /**
     * Get a new fraction when dividing the called fraction by the given fraction.
     *
     * @param fraction to divide this by.
     * @return result of the division.
     * @throws IllegalArgumentException when denominator is 0
     */
    public Fraction divideBy(final Fraction fraction) {
        return multiplyBy(fraction.flip());
    }

    /**
     * Get the reciprocal fraction.
     * <p/>
     * Will throw an IllegalArgumentException when called on 0.
     *
     * @return reciprocal fraction of the called fraction
     * @throws IllegalArgumentException when numerator is 0
     */
    public Fraction flip() {
        return new Fraction(denominator, numerator);
    }

    /**
     * Print the fraction in the format numerator/denominator.
     */
    public String toString() {
        return (denominator == 1) ? String.format("%d", numerator) : String.format("%d/%d", numerator, denominator);
    }


    @Override
    public boolean equals(final Object obj) {

        if (obj instanceof Fraction) {
            final Fraction that = (Fraction) obj;

            return numerator == that.numerator && denominator == that.denominator;
        }

        return false;
    }

    @Override
    public int hashCode() {
        int result = numerator;
        result = 31 * result + denominator;
        return result;
    }

    private void transformToLowestTerms() {
        final int gcd = gcd(numerator, denominator);
        this.numerator = this.numerator / gcd;
        this.denominator = this.denominator / gcd;
    }


    private static int gcd(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }
}